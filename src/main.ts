import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import VueApollo from 'vue-apollo';
import ApolloClient from 'apollo-boost';
import {ACCESS_TOKEN} from '@/config/config';

Vue.config.productionTip = false;
Vue.use(VueApollo);
// @ts-ignore
const client = new ApolloClient({
  uri: 'https://api.github.com/graphql',
  request: (operation) => {
    operation.setContext({
      headers: {
        authorization: `Bearer ${ACCESS_TOKEN}`,
      },
    });
  },
});

const apolloProvider = new VueApollo({
  defaultClient: client,
});


new Vue({
  router,
  store,
  render: (h) => h(App),
  apolloProvider,
}).$mount('#app');
