import {gql} from 'apollo-boost';

export const GET_REPOS = gql`
  query {
          viewer {
            repositories(first: 10) {
              edges {
                node {
                  id
                  name,
                  url,
                  description
                }
              }
            }
          }
    }`;

export const GET_FORTUMO_REPOS = gql`
  query($loginName: String!) {
    repositoryOwner(login: $loginName) {
      repositories(first: 50) {
        edges {
          node {
            id
            name,
            url,
            description
          }
        }
      }
    }
  }
`;

